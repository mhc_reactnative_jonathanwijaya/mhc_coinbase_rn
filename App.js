import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, StatusBar} from 'react-native';
import { Provider } from 'react-redux'

//Routes
import Routes from "./src/components/routes/routes"

//Initialize Redux
import configureStore from "./src/redux/store";
const store = configureStore();

class SplashScreen extends Component{
  render(){
    return (
      <View
      style={{
        flex: 1,
        backgroundColor: "#fafafa",
        justifyContent: "center",
        alignItems: "center"
      }}>
      <StatusBar backgroundColor="#fafafa"/>
      <Image
        style={styles.splashLogo}
        source={require("./src/images/logo.png")}
      />
      <Text style={styles.splashText}>Cryptocurrency Market Capitalizations</Text>
      </View>
    )
  }
}

export default class App extends Component {
  constructor(props){
    super(props);

    this.state = {isLoading : true};
  }

  performTimeConsumingTask = async () => {
    return new Promise(resolve =>
      setTimeout(()=> {
        resolve("result");
      },2000)
      );
  };

  async componentDidMount(){
    const data= await this.performTimeConsumingTask();
    
    if (data !== null){
      this.setState({ isLoading:false });
    }
  }

  render() {
    if (this.state.isLoading){
      return <SplashScreen/>
    }
    return (
      <Provider store={store}>
      <View style={styles.container}>
        <StatusBar backgroundColor="#1d1e21"/>
        <Routes/>
      </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  splashText:{
    color: "#1d1e21"
  },
  splashLogo:{
    width: 320,
    height: 55
  }
});
