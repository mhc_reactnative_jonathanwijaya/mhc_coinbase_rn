import types from '../action/type';

const initialState = {
    coins: [],
    errorLog: {},
    filterKey: '',
  };
  
  export default function(state = initialState, action) {
    switch (action.type) {
    
      case types.SUCCESS_LIST_COINS:{
        return {
          ...state,
          coins: action.coins
        };
      }
      case types.SEARCH_LIST_COINS:{
      return {
        // portofolios: state.portofolios,
        // errorLog: state.errorLog,
        ...state,
        filterKey: action.event.filterKey
      };
    }
      case types.FAILED_LIST_COINS: {
        return {
          ...state,
          errorLog: {
            ...state.errorLog,
            coin: action.error
          }
        };
      }
      case types.RESET_LIST_COINS: {
        return {
          state: initialState
        };
      }
      default:
        return {
          ...state
        };
    }
  }
  