import { combineReducers } from "redux";
import listCoinsReducer from './listCoinsReducer';

export default combineReducers({
    listCoins: listCoinsReducer
})