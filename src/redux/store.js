import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleWare from "redux-saga";

import rootSaga from './saga';
import reducer from './reducer';

const initialState={};

const configureStore = () => {
    const sagaMiddleWare = createSagaMiddleWare();

    return{
        ...createStore(reducer, initialState, applyMiddleware(sagaMiddleWare)),
        runSaga: sagaMiddleWare.run(rootSaga)
    };
};

export default configureStore;