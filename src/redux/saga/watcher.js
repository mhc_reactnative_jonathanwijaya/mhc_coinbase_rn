import { takeLatest } from "redux-saga/effects";

import listCoinsSaga from "./listCoinsSaga";

import type from "../action/type";

export default{
    *watchListCoinsSaga(){
        yield takeLatest(type.FETCH_LIST_COINS,listCoinsSaga.fetchListCoins);
    },
};