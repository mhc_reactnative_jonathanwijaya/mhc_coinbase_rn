import { call, put } from "redux-saga/effects";
import { fetchListCoin} from '../action/listCoinsAction';
import types from '../action/type';

export default {
    // component => saga
    // two parameters:
    // 1. type
    // 2. body
    *fetchListCoins() {
      try {
        const result = yield call(
          fetchListCoin
        );
        if (result.status === 200) {
          yield put({
            type: types.SUCCESS_LIST_COINS,
            coins: result.data.data
          });
        } else if (!result.success) {
          yield put({
            type: types.FAILED_LIST_COINS,
            error: result.message
          });
        }
      } catch (error) {
        yield put({
          type: types.FAILED_LIST_COINS,
          error
        });
      }
    }
  };
  
