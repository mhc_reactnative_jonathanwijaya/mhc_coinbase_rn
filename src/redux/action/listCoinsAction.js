import axios from "axios";
import type from "./type";

const URL = `${type.ROOT_URL}v1/cryptocurrency/listings/latest`;

const fetchListCoin = async () => {
  try {
    const response = await axios.get(URL, {
      headers: { "X-CMC_PRO_API_KEY": type.API_KEY }
    });
    return response;
   
  } catch (error) {

    return error;
  }
};

export  {
  fetchListCoin
};
