import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  TextInput
} from "react-native";
import {
  Left,
  Container,
  Button,
  Header,
  Title,
  Body
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Actions } from  "react-native-router-flux"
import Drawer from "react-native-drawer";
import SideBar from "../drawer";

import axios from "axios";
//redux
import type from "../../redux/action/type";
import { connect } from "react-redux";
import lodash from "lodash";

class Homepage extends Component {
  
  componentDidMount() {
    const{ dispatch } = this.props;
    dispatch({
      type: type.FETCH_LIST_COINS
    });
  }

  handleSearch(filterKey){
    const { dispatch } = this.props;
    dispatch({
      type: type.SEARCH_LIST_COINS,
      event: {filterKey}
    });
  }

  information(id){
    Actions.info({id:id});
  }

  closeDrawer() {
    this._drawer.close();
  }
  openDrawer() {
    this._drawer.open();
  }

  shouldComponentUpdate(nextProps) {
    const { coins: prevCoins, filterKey: prevKey } = this.props;
    const { coins: nextCoins, filterKey: nextKey } = nextProps;
    return !lodash.isEqual(prevCoins, nextCoins) || !lodash.isEqual(prevKey, nextKey);
  }

  render() {

    const { coins, filterKey } = this.props;
    const filteredCoins = coins.filter(data => data.name.toLowerCase()
    .includes(filterKey));
    let renderLists;
    if(filteredCoins){ 
       renderLists = filteredCoins.map(cryptoList => {
      return (
        <TouchableOpacity onPress={() => this.information(cryptoList.id)}>
          <View style={styles.containerList}>
            <View style={styles.logoName}>
              <Image
                style={styles.logoImage}
                source={{
                  uri: `https://s2.coinmarketcap.com/static/img/coins/64x64/${
                    cryptoList.id
                  }.png`
                }}
              />
              <Text style={styles.titleText1}>{cryptoList.name}</Text>
            </View>
            <View style={styles.priceSupply}>
            <View style={{flexDirection:'row',alignItems:'center', paddingLeft:10}}>
            <Icon name="money-bill-wave" style={styles.icon}/> 
              <Text style={styles.titleText2}>
                USD $ {cryptoList.quote.USD.price}
              </Text>
              </View>
              <View style={{flexDirection:'row',alignItems:'center', paddingLeft:10}}>
              <Icon name="database" style={styles.icon}/>
              <Text style={styles.titleText2}>
                 {cryptoList.circulating_supply}
              </Text>
              </View>
            </View>
          </View>
          <View style={styles.containerListPercent}>
                <Text style={
                cryptoList.quote.USD.percent_change_1h >= 0
                  ? styles.percentPos
                  : styles.percentNeg
              }>
                 1H: {cryptoList.quote.USD.percent_change_1h}%
                </Text>
                <Text style={
                cryptoList.quote.USD.percent_change_24h >= 0
                  ? styles.percentPos
                  : styles.percentNeg
              }>
                 1D: {cryptoList.quote.USD.percent_change_24h}%
                </Text>
                <Text style={
                cryptoList.quote.USD.percent_change_7d >= 0
                  ? styles.percentPos
                  : styles.percentNeg
              }>
                  7D: {cryptoList.quote.USD.percent_change_7d}%
                </Text>
              </View>
        </TouchableOpacity>
      );
    });
  }
    return (
      <Drawer
        ref={ref => (this._drawer = ref)}
        type="static"
        content={<SideBar />}
        tapToClose={true}
        openDrawerOffset={100}
        styles={drawerStyles}
        tweenHandler={Drawer.tweenPresets.parallax}
      >
        <Container>
          <Header
            androidStatusBarColor="#1d1e21"
            style={{ backgroundColor: "#1d1e21" }}
          >
            <Left>
              <Button transparent onPress={() => this.openDrawer()}>
                <Icon name="bars" style={{fontSize:20,color:'#fafafa'}} />
              </Button>
            </Left>
            <Body>
              <Title>Homepage</Title>
            </Body>
          </Header>
          
        <View
          style={{
            height: 60,
            backgroundColor: "#1d1e21",
            justifyContent: "center",
            alignItems: "center",
            paddingHorizontal: 5,
            flexDirection: "row"
          }}
        >
          <Icon
            name="search"
            style={{
              fontSize: 24,
              paddingLeft: 5,
              marginLeft: 10,
              color: "#fff"
            }}
          />
          <TextInput
            placeholder="Search for CryptoCurrencies"
            placeholderTextColor="white"
            style={{
              fontSize: 18,
              marginLeft: 15,
              flex: 1,
              backgroundColor: "rgba(45, 45, 45, 1)",
              color: "white",
              paddingHorizontal: 20
            }}
            onChangeText={ filterKey => this.handleSearch(filterKey)}
          />
        </View>
      
          <Container style={styles.container}>
            <ScrollView>{renderLists}</ScrollView>
          </Container>
        </Container>
      </Drawer>
    );
  }
}

const drawerStyles = {
  drawer: { shadowColor: "#000000", shadowOpacity: 0.8, shadowRadius: 3 }
};

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'rgba(45,45,45,0.9)'
  },
  touchableContainer: {
    // marginTop: 5,
    justifyContent: "center",
    alignItems: "center",
    width: "50%"
  },
  containerList: {
    backgroundColor: "#fafafa",
    flex: 1,
    marginTop: 5,
    marginHorizontal: 5,
    flexDirection: 'row',
    borderTopLeftRadius: 10
  },
  containerListPercent: {
    backgroundColor: "#fafafa",
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: 5,
    borderBottomRightRadius: 10,
  },
  titleText1: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: 20,
    color: "#1d1e21",
    fontWeight: "bold"
  },
  titleText2: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: 17,
    color: "#1d1e21",
    marginLeft: 5,
  },
  percentText: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: 17,
    color: "#1d1e21",
    marginHorizontal:3
  },
  logoImage: {
    width: 50,
    height: 50,
  },
  logoName:{
    maxWidth: 125,
    width: '100%',
    maxHeight: 125,
    height: '100%',
    alignItems: 'center',
    paddingTop: 5,
    backgroundColor: '#60bc9f',
    borderTopLeftRadius: 10,
  },
  priceSupply:{
    justifyContent:'center',
    backgroundColor: '#4fa88c',
    flex:1,
    borderBottomRightRadius: 10,
  },
  percentPos: {
    paddingHorizontal: 5,
    color: "green",
  },
  percentNeg: {
    paddingHorizontal: 5,
    color: "red",
  },
  icon: {
    maxWidth: 25,
    width: "100%",
    color: "#1d1e21"
  },
});

const mapStateToProps = state => ({
  coins: state.listCoins.coins,
  errorLog: state.listCoins.errorLog,
  filterKey: state.listCoins.filterKey,

});

export default connect(mapStateToProps)(Homepage);
