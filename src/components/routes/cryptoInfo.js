import React, { Component } from "react";
import { Text, View, ScrollView, StyleSheet, Image } from "react-native";
import { Header, Container, Left, Button, Body, Title } from "native-base";
import axios from "axios";
import Icon from "react-native-vector-icons/FontAwesome5";

//redux
import type from "../../redux/action/type";
import { connect } from "react-redux";
import lodash from "lodash";

class cryptoInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lists: []
    };
  }

  shouldComponentUpdate(nextProps) {
    const { coins: prevCoins } = this.props;
    const { coins: nextCoins } = nextProps;
    return !lodash.isEqual(prevCoins, nextCoins);
  }

  render() {
    const { coins } = this.props;
    let renderLists;
    if (coins) {
      renderLists = coins
        .filter(data => data.id === this.props.id)
        .map(cryptoList => {
          return (
            <View style={styles.containerInformation}>
              <View style={styles.logoName}>
                <Image
                  style={styles.logoImage}
                  source={{
                    uri: `https://s2.coinmarketcap.com/static/img/coins/64x64/${
                      cryptoList.id
                    }.png`
                  }}
                />
                <Text
                  style={{ fontWeight: "bold", fontSize: 20, color: "#1d1e21" }}
                >
                  {cryptoList.name}
                </Text>
              </View>
              <View style={styles.profit}>
                <Text
                  style={{ color: "#1d1e21", fontWeight: "bold", fontSize: 12 }}
                >
                  %Change:
                </Text>
                <Text
                  style={
                    cryptoList.quote.USD.percent_change_1h >= 0
                      ? styles.percentPos
                      : styles.percentNeg
                  }
                >
                  1H {cryptoList.quote.USD.percent_change_1h} %
                </Text>
                <Text
                  style={
                    cryptoList.quote.USD.percent_change_24h >= 0
                      ? styles.percentPos
                      : styles.percentNeg
                  }
                >
                  24H {cryptoList.quote.USD.percent_change_24h} %
                </Text>
                <Text
                  style={
                    cryptoList.quote.USD.percent_change_7d >= 0
                      ? styles.percentPos
                      : styles.percentNeg
                  }
                >
                  7D {cryptoList.quote.USD.percent_change_7d} %
                </Text>
              </View>
              <View style={styles.informationBackground}>
                <View style={styles.market}>
                  <Icon name="trophy" style={styles.iconThrophy} />
                  <Text style={styles.textStyles}>#{cryptoList.cmc_rank} </Text>
                </View>
                <View style={styles.market}>
                  <Icon name="money-bill-wave" style={styles.iconMoney} />
                  <Text style={styles.textStyles}>
                    USD $ {cryptoList.quote.USD.price}
                  </Text>
                </View>
                <View style={styles.market}>
                  <Icon name="clock" style={styles.iconClock} />
                  <Text style={styles.textStyles}>
                    Last updated : {cryptoList.quote.USD.last_updated}
                  </Text>
                </View>
                <View style={styles.market}>
                  <Icon name="trademark" style={styles.iconTM} />
                  <Text style={styles.textStyles}>{cryptoList.symbol}</Text>
                </View>
                <View style={styles.market}>
                  <Icon name="globe" style={styles.iconGlobe} />
                  <Text style={styles.textStyles}>
                    Market Cap : $ USD {cryptoList.quote.USD.market_cap}
                  </Text>
                </View>
                <View style={styles.market}>
                  <Icon name="database" style={styles.iconData} />
                  <Text style={styles.textStyles}>
                    Volume :{" "}
                    {cryptoList.quote.USD.volume_24h}
                  </Text>
                  <Text style={{color:"#1d1e21"}}>
                    {" "}Last 24<Icon name="clock" />
                  </Text>
                </View>
                <View style={styles.market}>
                  <Icon name="balance-scale" style={styles.iconScale} />
                  <Text style={styles.textStyles}>
                    Circulating : {cryptoList.circulating_supply} /
                  </Text>
                  <Text style={styles.marketFill}>{cryptoList.max_supply}</Text>
                </View>
                <View style={styles.market}>
                  <Icon name="piggy-bank" style={styles.iconPig} />
                  <Text style={styles.textStyles}>{cryptoList.tags}</Text>
                </View>
                <View style={styles.market}>
                  <Icon name="calendar-plus" style={styles.iconCalendar} />
                  <Text style={styles.textStyles}>
                    Date added : {cryptoList.date_added}
                  </Text>
                </View>
              </View>
            </View>
          );
        });
    }
    return (
      <Container>
        <Header
          androidStatusBarColor="#1d1e21"
          style={{ backgroundColor: "#1d1e21" }}
        >
          <Left>
            <Icon
              style={{ color: "#fafafa", paddingLeft: 5, fontSize: 20 }}
              name="info-circle"
            />
          </Left>
          <Body>
            <Title>Currency Information</Title>
          </Body>
        </Header>
        <ScrollView style={styles.container}>{renderLists}</ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(45,45,45,0.9)"
  },
  logoImage: {
    width: 50,
    height: 50
  },
  containerInformation: {
    backgroundColor: "#fafafa",
    margin: 5,
    padding: 5,
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  logoName: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#60bc9f",
    paddingTop: 5,
    borderTopLeftRadius: 10
  },
  profit: {
    flexDirection: "row",
    padding: 5,
    alignItems: "center",
    justifyContent: "space-between"
  },
  percentPos: {
    paddingHorizontal: 5,
    color: "green",
    fontSize: 12
  },
  percentNeg: {
    paddingHorizontal: 5,
    color: "red",
    fontSize: 12
  },
  market: {
    flexDirection: "row",
    padding: 5,
    alignItems: "center"
    // justifyContent: 'center'
  },
  iconThrophy: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#FFD700"
  },
  iconMoney: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#F0E68C"
  },
  iconClock: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#ce7d0c"
  },
  iconTM: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#1d1e21"
  },
  iconGlobe: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#4ec9e8"
  },
  iconData: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#b4c3c6"
  },
  iconScale: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#cd7f32"
  },
  iconPig: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#FCD7DE"
  },
  iconCalendar: {
    fontSize: 17,
    maxWidth: 30,
    width: "100%",
    color: "#fafafa"
  },
  marketFill: {
    paddingHorizontal: 2,
    fontWeight: "bold",
    color: "#1d1e21"
  },
  informationBackground: {
    backgroundColor: "#4fa88c",
    borderBottomRightRadius: 10
  },
  textStyles: {
    fontWeight: "bold",
    color: "#1d1e21"
  }
});

const mapStateToProps = state => ({
  coins: state.listCoins.coins,
  errorLog: state.listCoins.errorLog
});

export default connect(mapStateToProps)(cryptoInfo);
