import React, { Component } from 'react'
import { Router, Stack, Scene } from 'react-native-router-flux';
import { connect } from "react-redux";
import _ from "lodash";
//Components
import homepage from './homepage';
import login from './login';
import search from './search';
import cryptoInfo from './cryptoInfo';


class Routes extends Component {
  render() {
    return (
      <Router>
          <Stack key="root" hideNavBar>
            <Scene key="homepage" component={homepage} title='Homepage' type="replace"/>
            <Scene key="login" component={login} title ='login' type="replace" initial/>   
            <Scene key="search" component={search} title='search'/>
            <Scene key="info" component={cryptoInfo} title='information' />
          </Stack>
      </Router>
    )
  }
}

export default Routes;