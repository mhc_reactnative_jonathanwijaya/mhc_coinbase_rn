import React, { Component } from "react";
import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import Reinput from "reinput";
import { Actions } from 'react-native-router-flux'

export default class login extends Component {

    homepage(){
        Actions.homepage();
    }
    
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.image}
            source={require("../../images/loginlogo.png")}
          />
        </View>
        <View style={styles.reinput}>
          <Reinput
            label="Username/ E-mail Address"
            labelColor="#1d1e21"
            labelActiveColor="#1d1e21"
            labelActiveScale={1}
            placeholderColor="#1d1e21"
            underlineColorAndroid="#1d1e21"
            placeholderTextColor="#1d1e21"
            underlineActiveColor="#1d1e21"
            keyboardType="email-address"
            autoCapitalize="none"
            returnKeyType="next"
            color="#1d1e21"
            ref={(input => this.emailInput =input)}
            onSubmitEditing={() => this.passwordInput.focus()}
          />
          <Reinput
            label="Password"
            labelColor="#1d1e21"
            labelActiveColor="#1d1e21"
            labelActiveScale={1}
            underlineActiveColor="#1d1e21"
            placeholderColor="#1d1e21"
            keyboardType="name-phone-pad"
            autoCapitalize="none"
            secureTextEntry
            returnKeyType="next"
            color="#1d1e21"
            ref={(input => this.passwordInput =input)}
          />
        </View>
        <View style={styles.loginbutton}>
          <TouchableOpacity style={styles.loginStyling} onPress={()=>this.homepage()}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>
        </View>
        {/* <View style={styles.footer}>
          <TouchableOpacity style={styles.footerF}>
            <Text style={{color:'#1d1e21'}}>Forget Password?</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.footerF}>
            <Text style={{color:'#1d1e21'}}>Sign Up Here</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: "center",
    backgroundColor: "#fafafa",
  },
  image: {
    width: 250,
    height: 250,
  },
  loginStyling: {
    width: 300,
    height: 60,
    backgroundColor: "#1d1e21",
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  loginText: {
    color: "#fafafa",
    fontWeight: "bold"
  },
  logoContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex : 1,
    maxHeight: 200,
    height: '100%',
    paddingTop: 20
  },
  reinput: {
    justifyContent: "center",
    alignItems: "center",
    width: 300,
    margin: "auto",
  },
  loginbutton: {
    flex: 1,
    justifyContent: "center",
    maxHeight: 100,
    height: '100%'
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
    minWidth: 300,
    maxWidth: 300,
    minHeight: 60,
    maxHeight: 60,
  },
  footerF: {
    alignItems: "center",
    justifyContent: 'center',
    textAlign: "center",
    fontSize: 12,
  },
});
