import React, { Component } from "react";
import {
  AppRegistry,
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { Icon } from "native-base";
import { Actions } from "react-native-router-flux";

export default class drawer extends Component {

    logout(){
        Actions.login();
    }

    search(){
        Actions.search();
    }

    homepage(){
      Actions.homepage();
    }

    render() {
    return (
      <View style={styles.container}>
      <Image
          style={styles.bg}
          resizeMode="cover"
          source={require("../images/wallpaper.jpg")}
          // blurRadius={1.5}
        />
        <View style={styles.drawerHeader}>
          <Image
            style={styles.avatar}
            source={require("../images/avatar.png")}
          />
          
          <Text style={styles.avatarText}>Jonathan Wijaya</Text>
        </View>
        <View>
          <TouchableOpacity style={styles.touchable} onPress={()=>this.homepage()}>
            <Icon style={styles.icon} name="home" />
            <Text style={styles.drawerText}> Home </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.footer}>
        <TouchableOpacity style={styles.touchable}>
          <Icon style={styles.icon} name="help" />
          <Text style={styles.drawerText}> Help & feedback</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.touchable} onPress={()=>this.logout()} >
          <Icon style={styles.icon} name="exit"/>
          <Text style={styles.drawerText}> Logout </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fafafa"
  },
  drawerHeader: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#1d1e21",
    maxHeight: 150,
    height: "100%",
    justifyContent: "space-around",
    alignItems: "center"
  },
  avatar: {
    width: 100,
    height: 100
  },
  avatarText: {
    fontWeight: "bold",
    color: "#fafafa"
  },
  footer: {
    flex: 1,
    justifyContent: "flex-end",
    paddingTop: 10
  },
  icon:{
    color:"#1d1e21"
  },
  touchable:{
      flexDirection:'row',
      alignItems: 'center',
      padding: 20,
      backgroundColor: 'rgba(250,250,250,0.9)'
  },
  drawerText:{
      fontWeight: '800',
      paddingLeft: 20,
      color:"#1d1e21"
  },
  bg: {
    zIndex: 0,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 1,
    maxWidth: 1000,
    width:'100%',
    maxHeight: 680,
    height:'100%',
  },
});
