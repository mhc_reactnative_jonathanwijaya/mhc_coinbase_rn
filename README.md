**React Native CoinMarketCap Application Documentation**

In this readme, readers will be guided to install and step-by-step on how to use the application build by the candidate.

Steps: 

1. Download the apk available from this link *'https://drive.google.com/file/d/1feQxG85RL8fHG-g7x4Ye_aSL4pMYnbpf/view'*

2. Install the apk into android mobile with Android Version 5.1 and above !

3. After finished installing run the application which is coinbase.

![alt_text](screenshots/screen1.png)

*User will be greeted with splashscreen of the android application*

![alt_text](screenshots/screen2.png)

*This login page is just a mock up of login interface, which means any credentials is not necessary. Tap on the Login Button.*

**List of All Available Coins**

![alt_text](screenshots/screen3.png)

**Search Available Coins Utilizing SearchBar**

![alt_text](screenshots/screen4.png)

*User will be redirected to the homepage where it will list all of the available coins from coinmarketcap.com API. In this page user can choose one of the available coins from the lists or just search the desired information of the coins by using searchbar.*

![alt_text](screenshots/screen5.png)

*Example of the displayed information of the coins is displayed as below.*

![alt_text](screenshots/screen6.png)

*By using the drawer available user can log-out from the application.*

*Voila you managed to use this simple apps!*

**Example Video of The Application Usage**

![alt_text](screenshots/tutorial.gif)

--------------------------------------------------------------------------------------------------------

**Explanation on The Implementation of The Application**

1. This application code is wrote using visual studio code and bitbucket as the repository.

2. The framework used for this application is ReactNative

3. Several libraries used are:
-axios *for HTTP request*
-native-base *UI Component*
-reinput *TextInput with animation*
-react-native 
-react-native-router-flux *Routing*
-react-redux *State Management Tool*
-redux-saga *Middleware for react and redux*

